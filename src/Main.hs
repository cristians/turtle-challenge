module Main where
import System.IO (readFile)
import TurtleGame

main :: IO ()
main = do
  board <- readFile "game-files/board.json"
  turtle <- readFile "game-files/turtle.json"
  moves <- readFile "game-files/moves.json"
  putStrLn board
  putStrLn turtle
  putStrLn moves
