module TurtleGame where

data Point = Point { x:: Int, y :: Int} deriving (Show,Eq)
data Direction = North | East | South | West deriving (Show)

move :: Direction -> Point -> Point
move North (Point x y) =  Point x (y+1)
move East  (Point x y) =  Point (x+1) y
move South (Point x y) =  Point x (y-1)
move West  (Point x y) =  Point (x-1) y

rotate :: Direction -> Direction
rotate North = East
rotate East = South
rotate South = West
rotate West = North

data Command = Move | Rotate deriving (Show)
data Turtle = Turtle { location :: Point, facing :: Direction }

play :: Command -> Turtle -> Turtle
play Rotate turtle = Turtle (location turtle) (rotate $ facing turtle)
play Move turtle = Turtle newLocation (facing turtle)
  where
    newLocation = move (facing turtle) (location turtle)

type Mine = Point

data BoardSize = BoardSize
  { width :: Int
  , height :: Int
  }

data Board = Board
  { size :: BoardSize
  , exit :: Point
  , mines :: [Mine]
  }

data Outcome = Death | Success | Limbo deriving (Show,Eq)

isAt :: Point -> Point -> Bool
isAt a b = a == b

isAtMine :: [Mine] -> Point -> Bool
isAtMine mines position = any (isAt position) mines

eval :: Board -> Point -> Outcome
eval game position
  | isAtMine (mines game) position = Death
  | isAt (exit game) position = Success
  | otherwise = Limbo

playSequence :: Board -> Turtle -> [Command] -> Outcome
playSequence _ _ [] = Limbo
playSequence board turtle (i:is) =
  if current == Limbo then playSequence board turtle is else current
  where
    newTurtle = play i turtle
    current = eval board (location turtle)
